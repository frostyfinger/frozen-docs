# Project FrozenFist
## VortexOps

### Project Overview
You have just been hired as a summer intern at VortexOps. You've been assigned to work with the software development team, assisting the software architect.
The team is currently working on a project codenamed "Frozen Fist." They are designing software for semi-autonomous armored assault vehicles.
The vehicles are designed to operate either remotely with a human pilot or in "robot mode" with guidance from an on-board Artificial Intelligence (AI) system.

Feel free to contact [Kelly Morell](kelly.morell@smail.rasmussen.edu)with any questions.

![alt text](https://www.militaryimages.net/media/modern.61160/full "Frozen Fist")

### Pushed to BitBucket repository - 02-18-18 2nd time
### Modified README on BitBucket - 02-25-18