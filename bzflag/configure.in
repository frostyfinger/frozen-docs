# Process this file with autoconf to produce a configure script.

AC_INIT(src/bzflag/bzflag.cxx)

# sed magic to get version numbers from include/version.h
MAJOR_VERSION=`sed -e 's/#.*define.*BZ_MAJOR_VERSION[^0-9]*\(.*\)/\1/' -e t -e d < \$srcdir/include/version.h`
MINOR_VERSION=`sed -e 's/#.*define.*BZ_MINOR_VERSION[^0-9]*\(.*\)/\1/' -e t -e d < \$srcdir/include/version.h`
REV=`sed -e 's/#.*define.*BZ_REV[^0-9]*\(.*\)/\1/' -e t -e d < \$srcdir/include/version.h`

CONF_DATE=`date +%Y%m%d`
echo "BZFlag-$MAJOR_VERSION.$MINOR_VERSION.$REV.$CONF_DATE"
AC_CANONICAL_SYSTEM
AM_INIT_AUTOMAKE(bzflag, $MAJOR_VERSION.$MINOR_VERSION.$REV.$CONF_DATE)
AM_CONFIG_HEADER(include/config.h)

# Let the C++ code know about OS and user
AC_DEFINE_UNQUOTED(BZ_BUILD_OS, "$host_os", [BZFlag System Environment])
AC_DEFINE_UNQUOTED(BZ_BUILD_USER, "`whoami`", [User who compiled BZFlag])

#Checks for programs.
AC_PROG_MAKE_SET
AC_PROG_CC
CFLAGS=
AC_PROG_CXX
CXXFLAGS=
AC_PROG_LN_S
AC_PROG_RANLIB
AC_CHECK_PROG(HASPOD2MAN, pod2man, yes, no)
if test $HASPOD2MAN = no; then
	AC_MSG_ERROR(could not find pod2man)
fi

# Just for kicks
echo -n "checking for artificial intelligence for robots..." 1>&6
echo " warning: not found" 1>&6

#Checks for types
#AM_CHECK_TYPES(socklen_t)
#AC_CHECK_TYPES(socklen_t)

AC_TRY_COMPILE([#include <sys/types.h>
		#include <sys/socket.h>],
	       [socklen_t len = 42; return 0;],
               ac_cv_type_socklen_t=yes,
               ac_cv_type_socklen_t=no)
if test $ac_cv_type_socklen_t != yes; then
    AC_DEFINE(socklen_t, int, [if socklen_t is not defined, provide something useful])
else
    AC_DEFINE(HAVE_SOCKLEN_T, 1, [if socklen_t is defined, make note of it])
fi

AM_CONDITIONAL(LINUX, test x$host_os = xlinux-gnu)
AM_CONDITIONAL(BEOS, test x$host_os = xbeos)
AM_CONDITIONAL(PC,    test x$host_vendor = xpc)
AM_CONDITIONAL(PPC,   test x$host_vendor = xppc)

case $host_os in
       solaris*) solaris=true;;
       *) solaris=false;;
     esac
AM_CONDITIONAL(SOLARIS, $solaris)
# Write code to detect platform specialization
sun_wspro=false
sun_cc=false
sun_gcc=false

case $host_os in
     irix*) irix=true;;
     *) irix=false;;
esac
AM_CONDITIONAL(IRIX, $irix)

case $host_os in
     cygwin|mingw32|windows**) win32=true;;
     *) win32=false;;
esac
AM_CONDITIONAL(WIN32, $win32)

case $host_os in
   macos|darwin*) macos=true;;
   *) macos=false;;
esac

AM_CONDITIONAL(MACOS, $macos)

case $host_os in
   linux-gnu)
	case $host_vendor in
	     pc)  CONFIG='linux-i386';;
	     ppc) CONFIG='linux-ppc';;
	     *)   CONFIG='linux';;
	esac;;
   solaris*)
	if $sun_cc; then
	   CONFIG=solaris-cc;
	elif $sun_gcc; then
	   CONFIG=solaris-gcc;
	else
	   CONFIG=solaris;
	fi;;
   irix*)
	if test $host_cpu = mips64; then
	   CONFIG=irix-mips3;
	else
	   CONFIG=irix-mips2;
	fi;;
   macos|darwin*)
	CONFIG=macosx;;
   cygwin|mingw32|windows*)
	CONFIG=config-win32;;
esac
AC_SUBST(CONFIG)

AC_PATH_XTRA

LDFLAGS=$X_LIBS
case $host_os in
  solaris*)
    GLIBS="-lresolv -laudio $GLIBS";
    LIBS="-lsocket -lnsl -lresolv";
    LDFLAGS="$LDFLAGS -L/usr/demo/SOUND/lib";;
  irix*)
    GLIBS="-lXsgivc -lX11 -laudio $GLIBS";;
  beos)
    GLIBS=" -lmedia -lgame $GLIBS";
#    LIBS="-lbind -lsocket -lbe";;
    LIBS="-lbe";;
esac;

AC_CHECK_FUNC(getrlimit)

# for BeOS - old network stack don't have those libs ( move it in the case switch ?)
AC_CHECK_LIB(socket, socket)
AC_CHECK_LIB(bind, gethostent)

AC_CHECK_LIB(m, sqrtf)

# avoid using X11 in BeOS
if test $host_os != beos; then
  CPPFLAGS=
  if test x$no_x = x; then
    GLIBS="$X_PRE_LIBS -lX11 $EXTRA_LIBS $GLIBS"
  fi

  AC_CHECK_LIB(Xext, XextAddDisplay, [GLIBS="-lXext $GLIBS"], [], $GLIBS)

  if test $host_os = linux-gnu; then
    AC_CHECK_LIB(Xxf86vm, XF86VidModeGetAllModeLines, [GLIBS="-lXxf86vm $GLIBS"], [], $GLIBS)
    AC_CHECK_LIB(Xi, XListInputDevices, [GLIBS="-lXi $GLIBS"], [], $GLIBS)
  fi
fi

AC_LANG_CPLUSPLUS

AC_TRY_LINK([
#include <sys/types.h>
#include <regex.h>],
[regcomp(0, 0, 0);
 regexec(0, 0, 0, 0, 0)],
[AC_DEFINE(HAVE_REGEX_H, 1, [Define to 1 if you have regex stuff available])],
[AC_MSG_WARN([regexp/regcomp seems not being available on your system])])

ac_cv_search_glBegin=no
ac_cv_search_gluScaleImage=no
ac_func_search_save_LIBS=$LIBS

LIBS="-framework OpenGL $GLIBS $ac_func_search_save_LIBS"
AC_TRY_LINK([#include <OpenGL/gl.h>],
            [glBegin(GL_POINTS)],
            [ac_cv_search_glBegin="-framework OpenGL"])
for ac_lib in opengl32 GL2 GL; do
  LIBS="-l$ac_lib $GLIBS $ac_func_search_save_LIBS"
  AC_TRY_LINK([#include <GL/gl.h>],
              [glBegin(GL_POINTS)],
              [ac_cv_search_glBegin="-l$ac_lib"
               break])
done
if test "$ac_cv_search_glBegin" != no; then
  GLIBS="$ac_cv_search_glBegin $GLIBS"
  if test "$ac_cv_search_glBegin" = -lGL2; then
    AC_DEFINE(BEOS_USE_GL2, 1, [Use new GL Kit for BeOS])
  fi

  LIBS="-framework OpenGL $ac_func_search_save_LIBS"
  AC_TRY_LINK([#include <OpenGL/glu.h>],
              [gluScaleImage(GL_RED,0,0,GL_INT,0,0,0,GL_INT,0)],
              [ac_cv_search_gluScaleImage="-framework OpenGL"])
  for ac_lib in glu32 GLU; do
    LIBS="-l$ac_lib $GLIBS $ac_func_search_save_LIBS"
    AC_TRY_LINK([#include <GL/glu.h>],
                [gluScaleImage(GL_RED,0,0,GL_INT,0,0,0,GL_INT,0)],
		[ac_cv_search_gluScaleImage="-l$ac_lib"
		 break])
  done
  if test "$ac_cv_search_gluScaleImage" != no; then
    GLIBS="$ac_cv_search_gluScaleImage $GLIBS"
  fi
fi

LIBS=$ac_func_search_save_LIBS
AC_SUBST(GLIBS)

if test $host_os = mingw32; then
  LIBS="-lwinmm $LIBS"
  GLIBS="-mwindows -lgdi32 -lws2_32 -ldsound $LIBS"
fi

# Remove ogg/vorbis dependencies until we actually need them.
#
# AC_CHECK_LIB(ogg, ogg_stream_init, [ALIBS="-logg $ALIBS"], [], $ALIBS)
# AC_CHECK_LIB(vorbis, vorbis_info_init, [ALIBS="-lvorbis $ALIBS"], [], $ALIBS)
# AC_CHECK_LIB(vorbisfile, ov_open, [ALIBS="-lvorbisfile $ALIBS"], [], $ALIBS)
# AC_SUBST(ALIBS)

case $host_os in
     macos|darwin*)
	LIBS="$LIBS -framework Carbon -framework OpenGL -framework AGL"
	;;
     irix)
	if test x$host_cpu = xmips64; then
	   LDFLAGS="$LDFLAGS -L$(ROOT)/usr/lib32"
	else
	   LDFLAGS="$LDFLAGS -L$(ROOT)/usr/lib"
	fi
	;;
esac
if test $prefix = NONE; then
	prefix=$ac_default_prefix
fi
INSTALL_DATA_DIR=`eval echo $datadir/bzflag`
AC_SUBST(INSTALL_DATA_DIR)

# do we want bzadmin?
AC_ARG_ENABLE(bzadmin, [  --disable-bzadmin       do not build text client])
AM_CONDITIONAL(BZADMIN_INCLUDED, test x$enable_bzadmin != xno)
if test x$enable_bzadmin != xno; then
   MP_WITH_CURSES
   if test "x$CURSES_LIB" = x; then
      AC_MSG_WARN([could not find a curses library, will build bzadmin \
                  without curses])
   fi
fi
AM_CONDITIONAL(HAVE_CURSES, test "x$CURSES_LIB" != x)
AC_SUBST(CURSES_LIB)


AC_ARG_ENABLE(timebomb, [  --enable-timebomb       build exp Date])
if test x$enable_timebomb = xyes; then
   AC_DEFINE(TIME_BOMB, "3/13/1998", [Time Bomb expiration])
fi

AC_ARG_ENABLE(debug, [  --enable-debug          turn on debugging])
AM_CONDITIONAL(DEBUG, test x$enable_debug = xyes)
if test x$enable_debug = xyes; then
   AC_DEFINE(DEBUG_RENDERING, 1, [Debug Rendering])
   AC_ARG_ENABLE(profiling, [  --enable-profiling      turn on profiling])
fi

AC_ARG_ENABLE(client, [  --disable-client        servers-only build])
if test x$enable_client != xno; then
  if test "$ac_cv_search_gluScaleImage" = no; then
    AC_MSG_WARN(
    [Client build has been requested, but GL is not fully available
     ... disabling client generation])
    AM_CONDITIONAL(CLIENT_INCLUDED, false)
  else
    AM_CONDITIONAL(CLIENT_INCLUDED, true)
  fi
else
  AM_CONDITIONAL(CLIENT_INCLUDED, false)
fi

AC_ARG_ENABLE(robots, [  --disable-robots        disable robots])
if test x$enable_robots != xno; then
  AC_DEFINE(ROBOT, 1, [Enabling Robots])
fi

AC_ARG_ENABLE(snapping, [  --disable-snapping      disable snapping])
if test x$enable_snapping != xno; then
  AC_DEFINE(SNAPPING, 1, [Enabling Snapping])
fi

AC_DEFINE_UNQUOTED(INSTALL_DATA_DIR, "$INSTALL_DATA_DIR",
[Data file directory])
case $host_os in
     linux-gnu)
	CPPFLAGS="$CPPFLAGS -D_BSD_SOURCE -D_POSIX_SOURCE";
	CPPFLAGS="$CPPFLAGS -I\$(top_srcdir)/include"
	AC_DEFINE(HALF_RATE_AUDIO, 1, [Half rate Audio])
	AC_DEFINE(XF86VIDMODE_EXT, 1, [XFree86 Video Mode Extension])
	AC_DEFINE(XIJOYSTICK, 1, [XInput Joystick Handling])
	CPPFLAGS="$CPPFLAGS -Wall -W"
	CFLAGS="$CFLAGS -ansi -fno-exceptions";
	CXXFLAGS="$CXXFLAGS";
	case $host_vendor in
	     pc)  CPPFLAGS="$CPPFLAGS -mcpu=$host_cpu";;
	     ppc) CPPFLAGS="$CPPFLAGS -mcpu=$host_cpu";;
	esac;;
     cygwin)
	CPPFLAGS="$CPPFLAGS -D_BSD_SOURCE -D_POSIX_SOURCE";
	CPPFLAGS="$CPPFLAGS -I\$(top_srcdir)/include"
	AC_DEFINE(HALF_RATE_AUDIO, 1, [Half rate Audio])
	AC_DEFINE(XF86VIDMODE_EXT, 1, [XFree86 Video Mode Extension])
	AC_DEFINE(XIJOYSTICK, 1, [XInput Joystick Handling])
	CPPFLAGS="$CPPFLAGS -Wall -W -mcpu=$host_cpu"
	CFLAGS="$CFLAGS -ansi -fno-exceptions";
	CXXFLAGS="$CXXFLAGS";;
     mingw32)
	CPPFLAGS="$CPPFLAGS -D_POSIX_SOURCE";
	CPPFLAGS="$CPPFLAGS -I\$(top_srcdir)/include"
	AC_DEFINE(HALF_RATE_AUDIO, 1, [Half rate Audio])
	CPPFLAGS="$CPPFLAGS -Wall -W -mcpu=$host_cpu";
	CFLAGS="$CFLAGS -ansi -fno-exceptions";
	CXXFLAGS="$CXXFLAGS";;
     solaris*)
	AC_DEFINE(SUN_OGL_NO_VERTEX_MACROS, [], [Sun OpenGL No Macro Vertex])
	if $sun_wspro; then
	   CPPFLAGS="$CPPFLAGS -KPIC"
	elif $sun_cc; then
	   CPPFLAGS="$CPPFLAGS -fast"
	elif $sun_gcc; then
	   CXXFLAGS="$CXXFLAGS"
	CPPFLAGS="$CPPFLAGS -I\$(top_srcdir)/include"
	fi;;
     macos|darwin*)
	CPPFLAGS="$CPPFLAGS -I\$(top_srcdir)/include"
	CPPFLAGS="$CPPFLAGS -Wall -Wstrict-prototypes";
	CFLAGS="$CFLAGS -ansi -fno-exceptions -Wall -Wstrict-prototypes";
	CXXFLAGS="$CXXFLAGS";;
     windows**)
	CPPFLAGS="$CPPFLAGS /D \"WIN32\" /D \"_MBCS\"";
	CPPFLAGS="$CPPFLAGS /I \"\$(top_srcdir)/include\" /nologo /W3";;
     irix)
	CPPFLAGS="$CPPFLAGS -I$(ROOT)/usr/include -Wall";
	if test x$host_cpu = xmips64; then
	   CPPFLAGS="CPPFLAGS -march=mips3";
	else
	   CPPFLAGS="CPPFLAGS -march=mips2";
	fi;
esac

# /FD generates dependencies;  don't know where though
# /Fd names pdb file   /Fd"$(INTDIR)\\"
# /Fo names obj file   /Fo"$(INTDIR)\\"
# /D "_WINDOWS" when building windows app
# /D "_CONSOLE" when building console app
# /D "_LIB"     when building libraries
# /c to compile only
if test x$enable_debug = xyes; then
   case $host_os in
     linux-gnu|cygwin|mingw32)
	CFLAGS="$CFLAGS -g";
	CPPFLAGS="$CPPFLAGS -Werror";
	CXXFLAGS="$CXXFLAGS -g -Wno-deprecated";
	if test x$enable_profiling = xyes; then
	  CFLAGS="$CFLAGS -pg";
	  CXXFLAGS="$CXXFLAGS -pg";
	fi;;
     solaris*)
	CFLAGS="$CFLAGS -g";
	CXXFLAGS="$CXXFLAGS -g";;
     macos|darwin*)
	CFLAGS="$CFLAGS -g";
	CXXFLAGS="$CXXFLAGS -g";
	if test x$enable_profiling = xyes; then
	  CFLAGS="$CFLAGS -pg";
	  CXXFLAGS="$CXXFLAGS -pg";
	fi;;
     windows**)
	CPPFLAGS="$CPPFLAGS /MDd /Gm /ZI /Od /GZ /D \"_DEBUG\"";;
     irix)
	CFLAGS="$CFLAGS -g";
	CXXFLAGS="$CXXFLAGS -g";;
     beos)
	CFLAGS="$CFLAGS -g";
	CPPFLAGS="$CPPFLAGS -Werror";
	CXXFLAGS="$CXXFLAGS -g -Wno-deprecated";;
    esac;
    AC_DEFINE(DEBUG, 1, [Debugging enabled])
else
    case $host_os in
	linux-gnu|cygwin|mingw32)
		CFLAGS="$CFLAGS -O2";
		CXXFLAGS="$CXXFLAGS -O2";
		CXXFLAGS="$CXXFLAGS -ffast-math -fomit-frame-pointer";
		CXXFLAGS="$CXXFLAGS -fexpensive-optimizations -fno-exceptions";;
	 solaris*)
		CFLAGS="$CFLAGS -O";
		CXXFLAGS="$CXXFLAGS -O";;
	 macos|darwin*)
		CFLAGS="$CFLAGS -O3";
		CXXFLAGS="$CXXFLAGS -O3";
		CXXFLAGS="$CXXFLAGS -ffast-math -fomit-frame-pointer";
		CXXFLAGS="$CXXFLAGS -fexpensive-optimizations -fno-exceptions";
		if test x$enable_profiling = xyes; then
			CFLAGS="$CFLAGS -pg";
			CXXFLAGS="$CXXFLAGS -pg";
		fi;;
	 windows**)
		CPPFLAGS="$CPPFLAGS /MT /O2 ";;
	 irix)
		if test x$host_cpu = xmips64; then
		   CFLAGS="$CFLAGS -O3";
		   CXXFLAGS="$CXXFLAGS -O3";
		   CPPFLAGS="$CPPFLAGS -ffast-math -fomit-frame-pointer";
		   CPPFLAGS="$CPPFLAGS -fexpensive-optimizations -fno-exceptions";
		else
		   CFLAGS="$CFLAGS -O2";
		   CXXFLAGS="$CXXFLAGS -O2";
		fi;;
	 beos)
		CFLAGS="$CFLAGS -O2";
		CXXFLAGS="$CXXFLAGS -O2";
		CXXFLAGS="$CXXFLAGS -ffast-math -fomit-frame-pointer -fno-exceptions";;
     esac;
     AC_DEFINE(NDEBUG, 1, [Debugging disabled])
fi;


LIBDIR="\$(top_srcdir)/lib"
AC_SUBST(LIBDIR)

AC_OUTPUT(bzflag.spec
	bzflag.lsm
	Makefile
	data/Makefile
	data/l10n/Makefile
	debian/Makefile
	doc/Makefile
	doc/faq/Makefile
	doc/guide/Makefile
	doc/guide/Commands/Makefile
	doc/guide/Flags/Makefile
	doc/guide/Geometry/Makefile
	doc/guide/Installation/Makefile
	doc/guide/Tactics/Makefile
	doc/howto/Makefile
	doc/man/Makefile
	include/Makefile
	misc/Makefile
	man/Makefile
	package/Makefile
	package/linux/Makefile
	package/rpm/Makefile
	package/irix/Makefile
	package/mac/Makefile
	package/mac/notes/Makefile
	package/mac/platform/Makefile
	package/mac/src/Makefile
	package/win32/Makefile
	package/win32/include/Makefile
	package/win32/nsis/Makefile
	package/win32/src/Makefile
	package/win32/src/bzfinst/Makefile
	package/win32/src/bzfuinst/Makefile
	package/win32/src/makedb/Makefile
	src/Makefile
	src/bzadmin/Makefile
	src/bzflag/Makefile
	src/bzfrelay/Makefile
	src/bzfs/Makefile
	src/common/Makefile
	src/date/Makefile
	src/geometry/Makefile
	src/geometry/models/Makefile
	src/geometry/models/hitank/Makefile
	src/geometry/models/medtank/Makefile
	src/geometry/models/lowtank/Makefile
	src/mediafile/Makefile
	src/net/Makefile
	src/obstacle/Makefile
	src/ogl/Makefile
	src/platform/Makefile
	src/platform/MacOSX/Makefile
	src/platform/MacOSX/BZFlag.pbproj/Makefile
	src/platform/MacOSX/English.lproj/Makefile
	src/platform/MacOSX/English.lproj/MainMenu.nib/Makefile
	src/scene/Makefile
	src/zlib/Makefile
	win32/Makefile
	win32/VC6/Makefile
	win32/VC71/Makefile)
